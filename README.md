# JavaScript #

This tutorial is meant to give (at the minimum) a basic overview of JavaScript and its associated technologies for the purposes of CMPS 383 at Southeastern.

### What is JavaScript? ###

* Sparing you all the long, boring definitions, JavaScript is essentially a client-side language for use in web development.

* It is useful for its ability to be easy to learn, and efficient at tasks such as:

>Text matching (for passwords)

>Aesthetics (objects being highlighting on click)

>Object manipulation (Images moving across screen)

### DOM Manipulation ###

* The DOM, or Document Object Model is created when a web page is loaded into a browser

* It contains HTML elements and CSS styles that can all be changed or removed by using JavaScript

*DEMO HERE*

### AJAX ###

* AJAX, or "Asynchronous JavaScript and XML" is a technology used in tandem with JavaScript to actively update an element on a web page

* Prevents having to refresh the page

*DEMO HERE*

### Scoping and Closure ###

* Much like other programming languages, scope is an important topic to cover


